/**
 * Vorgehensmodelle
 */
public class Calculator {
/*
    //Round 1
    public static void main(String[] args) {
        try {
            if (!"+".equals(args[1])) {
                System.err.println("Unknown operation");
                return;
            }
            int result = Integer.parseInt(args[0]) + Integer.parseInt(args[2]);
            System.out.println("result = " + result);
        } catch (Exception e) {
            System.err.println("Available operations are: + - * /\n" +
                    "Usage example: 1 + 2\n");
        }
    }
*/

/*
    //Round 2
    public static void main(String[] args) {
        try {
            int x = Integer.parseInt(args[0]);
            int y = Integer.parseInt(args[2]);
            int result;

            Calculator calculator = new Calculator();
            if ("+".equals(args[1])) {
                result = calculator.addition(x, y);
            } else if ("/".equals(args[1])) {
                result = calculator.division(x, y);
            } else {
                System.err.println("Unknown operation");
                return;
            }

            System.out.println("result = " + result);
        } catch (Exception e) {
            System.err.println("Available operations are: + - * /\n" +
                    "Usage example: 1 + 2\n");
        }
    }

    public int addition(int x, int y) {
        return x + y;
    }

    public int division(int x, int y) {
        return x / y;
    }
*/

    //Round 3
    public static void main(String[] args) {
        String operation;
        int x, y;

        try {
            x = Integer.parseInt(args[0]);
            operation = args[1];
            y = Integer.parseInt(args[2]);
        } catch (Exception e) {
            System.err.println("Available operations are: + - * /\n" +
                    "Usage example: 1 + 2\n");
            return;
        }

        int result;

        try {
            Calculator calculator = new Calculator();
            result = calculator.calculate(operation, x, y);
        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
            return;
        }

        System.out.println(result);
    }

    public int calculate(String operation, int x, int y) throws Exception {
        switch (operation) {
            case "+":
                return addition(x, y);
            case "-":
                return subtraction(x, y);
            case "*":
                return multiplication(x, y);
            case "/":
                return division(x, y);
        }
        throw new Exception("Unknown operation");
    }

    public int addition(int x, int y) {
        return x + y;
    }

    public int subtraction(int x, int y) {
        return x - y;
    }

    public int multiplication(int x, int y) {
        return x * y;
    }

    public int division(int x, int y) {
        return x / y;
    }
}
