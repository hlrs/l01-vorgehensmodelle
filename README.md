## Lection 1 - Vorgehensmodelle ##

1. Beispiel 1 - Wassermodell
2. Beispiel 2 - V-Modell
3. Beispiel 3 - Spiralmodell
4. Beispiel 4 - Incremental
5. Beispiel 5 - Spiel

### Project structure ###

* `src` - source code
* `test` - tests source code
* `result` - runnable application

To run an application type in command line:

`
java -jar <application>.jar
`

### Contacts ###

IHR:

* "Natalia Currle-Linde" <linde@hlrs.de>
* "Yevgeniya Kovalenko" <kovalenko@hlrs.de>
* "Vyacheslav Gorbatykh" <hpcvgorb@hlrs.de>