package util;

import java.util.Objects;

/**
 */
public class TestUtil {
    public static void assertion(Object actual, Object expected, String operation) throws Exception {
        if (!Objects.equals(actual, expected)) {
            throw new Exception("Result of " + operation + " operation" +
                    "\n\tactual: " + actual +
                    "\n\texpected: " + expected
            );
        }
    }
}
