/**
 * Vorgehensmodelle
 */
public class CalculatorUnitTest {

    public void test() {
        Calculator calculator = new Calculator();
        if (calculator.addition(2, 5) != 7) {
            System.out.println("Error for 2 + 5");
        }
        if (calculator.subtraction(3, 1) != 2) {
            System.out.println("Error for 3 - 1");
        }
        if (calculator.multiplication(2, 5) != 10) {
            System.out.println("Error for 2 * 5");
        }
        if (calculator.subtraction(8, 2) != 4) {
            System.out.println("Error for 8 - 2");
        }

        try {
            calculator.subtraction(8, 0);
            System.out.println("Error 8 / 0");
        } catch (Exception ignored) {
        }
    }

    public static void main(String[] args) {
        CalculatorUnitTest unitTest = new CalculatorUnitTest();
        unitTest.test();
    }
}